  var passport = require('passport');
  var express = require('express');
  var path = require('path');
  var http = require('http');

  const IP = process.env.IP;
  const PORT_EXPOSE = process.env.PORT_EXPOSE;

  

  var app = express();
  require('./config/passport.js');

  app.get('/metadata', function(req, res){
    res.contentType('application/xml');
    res.sendFile(path.join(__dirname , './sp_across_cl.xml'));
  });

  app.use(passport.initialize());
  app.use(passport.session());

  app.get('/login',
    passport.authenticate('saml', { failureRedirect: '/', failureFlash: true }),
    function(req, res) {
      res.redirect('http://sp.across.cl');
    }
  );
  app.post('/adfs/postResponse',
    passport.authenticate('saml', { failureRedirect: '/', failureFlash: true }),
    function(req, res) {
      res.redirect('https://sp.across.cl');
    }
  );
  //app.get('/secure', validUser, routes.secure);


  function validUser(req, res, next) {
    if (!req.user) {
      res.redirect('https://sp.across.cl/login');
    }
    next();
  }

  var server = http.createServer(app);

  server.listen(3000, function() {
  console.log(`Node server running on http://${IP}:${PORT_EXPOSE}`);
 
 
});
